import utfpr.ct.dainf.if62c.avaliacao.Complexo;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Primeira avaliação parcial 2014/2.
 * @author 
 */
public class Avaliacao1 {

    public static void main(String[] args) {
        Complexo[] Raizes1 = new Complexo[2];
        Complexo[] Raizes2 = new Complexo[2];
        
        Raizes1 = raizesEquacao(new Complexo(1.0 ,0.0), new Complexo(5.0,0.0), new Complexo(4.0,0.0));
        Raizes2 = raizesEquacao(new Complexo(1.0 ,0.0), new Complexo(2.0,0.0), new Complexo(5.0,0.0));

        System.out.println("x1: " + Raizes1[0].toString());
        System.out.println("x2: " + Raizes1[1].toString());
        System.out.println("y1: " + Raizes2[0].toString());
        System.out.println("y2: " + Raizes2[1].toString());
        
    }
    
    public static Complexo[] raizesEquacao(Complexo a, Complexo b, Complexo c){
        Complexo[] RaizesEq = new Complexo[2];
        
        Complexo[] Delta = new Complexo[5];
        Complexo[] RaizesDelta = new Complexo[2];
        
        Delta[0] = b.prod(b);
        
        Delta[1] = a.prod(c);
        Delta[2] = Delta[1].prod(4.0);
        Delta[3] = Delta[0].sub(Delta[2]);
        
        RaizesDelta = Delta[3].sqrt();
                
        RaizesDelta[0].sub(b);
        RaizesDelta[1].sub(b);
        
        RaizesEq[0] = (RaizesDelta[0].sub(b)).div(a.prod(2.0)); 
        RaizesEq[1] = (RaizesDelta[1].sub(b)).div(a.prod(2.0));  
        
        return RaizesEq;
    }
    // implementar raizesEquacao(Complexo, Complexo, Complexo)
    
}

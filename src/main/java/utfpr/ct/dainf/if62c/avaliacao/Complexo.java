package utfpr.ct.dainf.if62c.avaliacao;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Primeira avaliação parcial 2014/2.
 * @author 
 */
public class Complexo {
    private double real;
    private double img;

    public Complexo() {
    }

    public Complexo(double real, double img) {
        this.real = real;
        this.img = img;
    }

    public double getReal(){
        return real;
    }
    
    public double getImg(){
        return img;
    }

    public Complexo soma(Complexo c) {
        return new Complexo(real + c.real, img + c.img);
    }
    
    public Complexo sub(Complexo c) {
        return new Complexo(real - c.real, img - c.img);//Estranho
    }

    public Complexo prod(Double n) {
        return new Complexo(real*n , img*n);
    }

    public Complexo prod(Complexo c) {
        return new Complexo(real*c.real - img*c.img, img*c.real + real*c.img);
    }
    
    public Complexo div(Complexo c) {
        return new Complexo( (real*c.real + img*c.img) / (c.real*c.real + c.img*c.img), (c.real*img - real*c.img) / (c.real*c.real + c.img*c.img) );
    }
    
    // implementar sqrt()
    public Complexo[] sqrt() {
        Complexo[] Raizes = new Complexo[2];
        
        double p, r;
        double fi;
        double m1, m2;
        
        if(real>0)
            fi = Math.atan(img/real);
        
        else if(real<0)
            fi = Math.atan(img/real) + Math.PI;
        
        else if(real==0 && img==0)
            fi = 0;
            
        else if(real==0 && img>0)
            fi = Math.PI/2;
        
        else
            fi = (3*Math.PI)/2;
        
        m1 = fi/2;
        m2= fi/2 + Math.PI;

        r = Math.sqrt(real*real + img*img);
        p = Math.sqrt(r);
        
        Raizes[0] = new Complexo( p*Math.cos(m1) , p*Math.sin(m1));
        Raizes[1] = new Complexo( p*Math.cos(m2) , p*Math.sin(m2));

        return Raizes;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + (int) (Double.doubleToLongBits(real)
            ^ (Double.doubleToLongBits(real) >>> 32));
        hash = 41 * hash + (int) (Double.doubleToLongBits(img)
            ^ (Double.doubleToLongBits(img) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        final Complexo c = (Complexo) obj;
        return obj != null && getClass() == obj.getClass()
            && real == c.real && img == c.img;
    }

    @Override
    public String toString() {
        return String.format("%+f%+fi", real, img);
    }
}
